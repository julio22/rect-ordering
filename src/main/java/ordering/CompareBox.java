package ordering;

import java.util.Comparator;

public class CompareBox implements Comparator<Integer[]> {

    enum Metric{
        L2,
        DX,
        DY
    }

    private Metric metric;

    public void setMetric(Metric metric) {
        this.metric = metric;
    }

    public Metric getMetric() {
        return metric;
    }

    public CompareBox() {
        metric = Metric.L2;
    }

    public int compare(Integer[] box_a, Integer[] box_b) {
        if (metric == Metric.L2){
            int r_a = box_a[0] * box_a[0] + box_a[1] * box_a[1];
            int r_b = box_b[0] * box_b[0] + box_b[1] * box_b[1];
            return r_a - r_b;
        }
        if (metric == Metric.DX) {
            return box_a[0] - box_b[0];
        }
        if (metric == Metric.DY){
            return box_a[1] - box_b[1];
        }
        return 0;
    }


    public boolean is_alignedY(Integer[] rect1, Integer[] rect2) {
        int y00 = rect1[1];
        int y01 = rect1[1] + rect1[3];
        int y10 = rect2[1];
        int y11 = rect2[1] + rect2[3];

        return is_aligned(y00, y01, y10, y11);
    }

    public boolean is_alignedX(Integer[] rect1, Integer[] rect2) {
        int x00 = rect1[0];
        int x01 = rect1[0] + rect1[2];
        int x10 = rect2[0];
        int x11 = rect2[0] + rect2[2];

        return is_aligned(x00, x01, x10, x11);
    }

    private boolean is_aligned(int p00, int p01, int p10, int p11) {
        float segment = Math.min((p01 - p00), (p11 - p10));
        if (p11 >= p00 && p11 <= p01) {
            if (p10 >= p00 && p10 <= p01)
                return true;
            else {
                float area_ratio = Math.abs(p11 - p00) / segment;
                return area_ratio > 0.5;
            }
        } else if (p10 >= p00 && p10 <= p01) {
            float area_ratio = Math.abs(p10 - p01) / segment;
            return area_ratio > 0.5;
        }
        return false;
    }
}
