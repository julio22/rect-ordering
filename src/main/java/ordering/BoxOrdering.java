package ordering;

import java.util.ArrayList;
import java.util.Collections;

public class BoxOrdering {
    public static ArrayList<Integer[]> sortElementsByAssociation(ArrayList<Integer[]> rects){
        ArrayList<Integer[]> sortedOutput = new ArrayList<>();
        ArrayList< ArrayList<Integer[]> > rowsList = new ArrayList<>();

        CompareBox compBox = new CompareBox();

        rects.sort(compBox);

        while (!rects.isEmpty()) {
            ArrayList<Integer[]> rowList = new ArrayList<>();
            Integer[] rectPivot = rects.get(rects.size()-1);
            rowList.add(rectPivot);
            for (int n=0; n<rects.size()-1; n++){
                Integer[] rect = rects.get(n);
                if (compBox.is_alignedY(rectPivot, rect)) {
                    rowList.add(rect);
                }
            }
            compBox.setMetric(CompareBox.Metric.DX);
            rowList.sort(compBox);
            rowsList.add(rowList);
            rects.removeAll(rowList);
        }

        ArrayList<Integer[]> indexOrdering = new ArrayList<>();
        int index = 0;
        for (ArrayList<Integer[]> row: rowsList) {
            Integer[] indexCreteria = new Integer[2];
            indexCreteria[0] = index++;
            int yMin = row.get(0)[1];
            for (Integer[] rect: row)
                if (rect[1] < yMin)
                    yMin = rect[1];
            indexCreteria[1] = yMin;
            indexOrdering.add(indexCreteria);
        }
        compBox.setMetric(CompareBox.Metric.DY);
        indexOrdering.sort(compBox);

        for (Integer[] criteria : indexOrdering) {
            index = criteria[0];
            ArrayList<Integer[]> rowList = rowsList.get(index);
            sortedOutput.addAll(rowList);
        }

        //Perform sorting on the positionData and add the data to sortedOutput
        return sortedOutput;
    }
}
