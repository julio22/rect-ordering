import java.util.ArrayList;

public class BoxOrdering {
    public static void main(String[] args) {
        ArrayList<Integer[]> boxes = new ArrayList<>();

        Integer[] b1 = {1,2,2,1};
        Integer[] b2 = {3,2,3,2};
        Integer[] b3 = {8,3,3,1};
        Integer[] b4 = {12,2,3,3};
        Integer[] b5 = {16,4,1,1};
        Integer[] b6 = {17,4,2,2};
        Integer[] b7 = {16,6,1,1};
        Integer[] b8 = {17,6,3,1};
        boxes.add(b1);
        boxes.add(b2);
        boxes.add(b3);
        boxes.add(b4);
        boxes.add(b5);
        boxes.add(b6);
        boxes.add(b7);
        boxes.add(b8);

        ArrayList<Integer[]> sortedBoxes = ordering.BoxOrdering.sortElementsByAssociation(boxes);
        for (Integer[] box: sortedBoxes)
            System.out.println("{" + box[0] + ", " + box[1] + ", " + box[2] + ", " + box[3] + "}");
    }
}
